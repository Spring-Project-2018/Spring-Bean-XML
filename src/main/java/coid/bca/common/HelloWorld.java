package coid.bca.common;

public class HelloWorld {
	
	private String name;
	private Integer n;
	
	public HelloWorld() { };
	
	public HelloWorld(String name) {
		this.name = name;
	}
	
	public HelloWorld(int n) {
		this.n = n;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setN(int n) {
		this.n = n;
	}
	
	public void printHello() {
//		if (name == null || name.isEmpty()) {
//			System.out.println("Hello World!");
//		}
//		else {
			System.out.println("Integer: " + n + " \t String: " + name);
//		}
	}
}
