package coid.bca.common;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext("ApplicationContext.xml");
		
		HelloWorld hello1 = (HelloWorld) context.getBean("helloBean3");
		HelloWorld hello2 = (HelloWorld) context.getBean("helloBean4");
		
		hello1.printHello();
		hello2.printHello();
	}

}
